package com.generalmagic.ptviewer.dbcreator;

import android.util.Log;

import com.generalmagic.ptviewer.utils.Bits;
import com.generalmagic.ptviewer.utils.TimeUtils;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

class GtfsParser
{
    private File baseDir;
    private Date currentDate;
    private Date currentEndDate;
    private Map<String, Agency> agenciesMap;
    private Map<String, Stop> stopsMap;
    private Map<String, BitSet> calendarsMap;
    private Map<String, Trip> tripsMap;
    private Map<String, Route> routesMap;
    private List<Route> routes;

    private final String TAG = GtfsParser.class.getName();

    public GtfsParser()
    {
        agenciesMap = new HashMap<>();
        stopsMap = new HashMap<>();
        calendarsMap = new HashMap<>();
        tripsMap = new HashMap<>();
        routesMap = new HashMap<>();
        routes = new ArrayList<>();
    }

    public boolean parse(File dir)
    {
        baseDir = dir;

        parseAgencies();
        parseStops();
        parseCalendars();
        parseCalendarDates();
        parseTrips();
        parseStopTimes();
        parseRoutes();

        doFinalProcessing();

        return true;
    }

    private void doFinalProcessing()
    {
        //sort stop times ascending in trips
        for(Trip trip : tripsMap.values())
            trip.sortStopTimes();

        //assign trips to routes

        routes.clear();

        for(Trip trip : tripsMap.values())
        {
            Route matchingRoute = routesMap.get(trip.getRouteGtfsId());

            if(matchingRoute == null)
            {
                Log.w(TAG, "Trip with no matching route: " + trip.getGtfsId());
                continue;
            }

            if(matchingRoute.hasNoTrips())
            {
                matchingRoute.addTrip(trip);
                routes.add(matchingRoute);
                continue;
            }

            if(matchingRoute.matchesTrip(trip))
            {
                matchingRoute.addTrip(trip);
            }
            else
            {
                Route matchRoute = null;

                for(Route route : routes)
                    if(route.matchesTrip(trip))
                    {
                        matchRoute = route;
                        break;
                    }

                if(matchRoute == null)
                {
                    matchRoute = matchingRoute.cloneRoute();
                    routes.add(matchRoute);
                }
                matchRoute.addTrip(trip);
            }
        }

        //sort trips inside a route
        for(Route r : routes)
            r.sortTrips();
    }

    public boolean parseAgencies()
    {
        Log.i(TAG, "Parsing agencies...");

        agenciesMap.clear();

        File agencyFile = new File(baseDir, "agency.txt");

        try
        {
            CSVParser parser = CSVParser.parse(agencyFile, Charset.forName("UTF-8") , CSVFormat.RFC4180.withHeader());

            for (final CSVRecord record : parser)
            {
                String agencyGtfsId = record.get("agency_id");
                String agencyName = record.get("agency_name");
                String url = record.get("agency_url");

                agenciesMap.put(agencyGtfsId, new Agency(agencyName, url));
            }

        }
        catch(IOException e)
        {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public boolean parseStops()
    {
        Log.i(TAG, "Parsing stops...");

        stopsMap.clear();

        File agencyFile = new File(baseDir, "stops.txt");

        try
        {
            CSVParser parser = CSVParser.parse(agencyFile, Charset.forName("UTF-8") , CSVFormat.RFC4180.withHeader());

            for (final CSVRecord record : parser)
            {
                String stopGtfsId = record.get("stop_id");
                String stopName = record.get("stop_name");
                double lat = Double.parseDouble(record.get("stop_lat"));
                double lon = Double.parseDouble(record.get("stop_lon"));

                stopsMap.put(stopGtfsId, new Stop(stopName, lat, lon));
            }

        }
        catch(IOException e)
        {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public boolean parseCalendars()
    {
        Log.i(TAG, "Parsing calendars and calendar dates...");

        tripsMap.clear();

        currentDate = TimeUtils.getCurrentDate();

        currentEndDate = TimeUtils.getDatePlusDays(currentDate, 32);

        int[] days = new int[7];

        File calendarFile = new File(baseDir, "calendar.txt");

        try
        {
            CSVParser parser = CSVParser.parse(calendarFile, Charset.forName("UTF-8") , CSVFormat.RFC4180.withHeader());

            for (final CSVRecord record : parser)
            {
                String serviceGtfsId = record.get("service_id");
                days[0] = Integer.parseInt(record.get("sunday"));
                days[1] = Integer.parseInt(record.get("monday"));
                days[2] = Integer.parseInt(record.get("tuesday"));
                days[3] = Integer.parseInt(record.get("wednesday"));
                days[4] = Integer.parseInt(record.get("thursday"));
                days[5] = Integer.parseInt(record.get("friday"));
                days[6] = Integer.parseInt(record.get("saturday"));
                String startDateStr = record.get("start_date");
                String endDateStr = record.get("end_date");

                Date startDate = TimeUtils.getDateFromStr(startDateStr);
                Date endDate = TimeUtils.getDateFromStr(endDateStr);

                Date actualStartDate = startDate.compareTo(currentDate) > 0 ? startDate : currentDate;
                Date actualEndDate = endDate.compareTo(currentEndDate) < 0 ? endDate : currentEndDate;

//                Log.i(TAG, serviceGtfsId + ":" + actualStartDate + "," + actualEndDate);

                BitSet bitSet = new BitSet(32);

                int day = TimeUtils.getDifferenceDays(currentDate, actualStartDate);
                int currentDayOfWeek = TimeUtils.getDayOfWeek(actualStartDate);

                while(actualStartDate.compareTo(actualEndDate) < 0)
                {
                    if(days[currentDayOfWeek] == 1)
                        bitSet.set(day);

                    actualStartDate = TimeUtils.getDatePlusDays(actualStartDate, 1);
                    day++;
                    currentDayOfWeek = (currentDayOfWeek + 1) % 7;
                }

                calendarsMap.put(serviceGtfsId, bitSet);
            }

        }
        catch(IOException e)
        {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public void parseCalendarDates()
    {
        Log.i(TAG, "Parsing calendars and calendar dates...");

        File calendarDatesFile = new File(baseDir, "calendar_dates.txt");

        try
        {
            CSVParser parser = CSVParser.parse(calendarDatesFile, Charset.forName("UTF-8"), CSVFormat.RFC4180.withHeader());

            for (final CSVRecord record : parser)
            {
                String serviceGtfsId = record.get("service_id");
                String dateStr = record.get("date");
                int exceptionType = Integer.parseInt(record.get("exception_type"));

                Date date = TimeUtils.getDateFromStr(dateStr);

                if (date.compareTo(currentDate) < 0 || date.compareTo(currentEndDate) >= 0)
                    continue;

                BitSet bs = calendarsMap.get(serviceGtfsId);

                if (bs == null)
                {
                    bs = new BitSet(32);
                    calendarsMap.put(serviceGtfsId, bs);
                }

                int day = TimeUtils.getDifferenceDays(currentDate, date);

                if (exceptionType == 1)
                    bs.set(day);
                else
                    bs.set(day, 0);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void parseTrips()
    {
        Log.i(TAG, "Parsing trips...");

        tripsMap.clear();

        File tripsFile = new File(baseDir, "trips.txt");

        try
        {
            CSVParser parser = CSVParser.parse(tripsFile, Charset.forName("UTF-8"), CSVFormat.RFC4180.withHeader());

            for (final CSVRecord record : parser)
            {
                String tripId = record.get("trip_id");
                String routeGtfsId = record.get("route_id");
                String serviceGtfsId = record.get("service_id");

                BitSet bs = calendarsMap.get(serviceGtfsId);
                if(bs == null)
                {
                    Log.w(TAG, "Non existent service for trip: " + serviceGtfsId);
                    continue;
                }

                Trip trip = new Trip(tripId, routeGtfsId, (int)Bits.convert(bs));

                tripsMap.put(tripId, trip);

            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void parseStopTimes()
    {
        Log.i(TAG, "Parsing stop times...");

        File stopTimesFile = new File(baseDir, "stop_times.txt");

        try
        {
            CSVParser parser = CSVParser.parse(stopTimesFile, Charset.forName("UTF-8"), CSVFormat.RFC4180.withHeader());

            for (final CSVRecord record : parser)
            {
                String tripId = record.get("trip_id");
                String arrivalStr = record.get("arrival_time");
                String departureStr = record.get("departure_time");
                String stopId = record.get("stop_id");
                int stopSequence = Integer.parseInt(record.get("stop_sequence"));

                int arrival = eightCharsTimeToSeconds(arrivalStr);
                int departure = eightCharsTimeToSeconds(departureStr);

                StopTime st = new StopTime(arrival, departure, stopId, tripId, stopSequence);

                Trip trip = tripsMap.get(tripId);

                if(trip == null)
                {
                    Log.w(TAG, "Non existent trip with id (when reading stop times): " + tripId);
                    continue;
                }

                trip.addStopTime(st);
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    public static int eightCharsTimeToSeconds(String time)
    {
        try
        {
            String[] split = time.trim().split(":");
            return Integer.parseInt(split[0]) * 3600 + Integer.parseInt(split[1]) * 60 + Integer.parseInt(split[2]);
        } catch (Exception e)
        {
            return -1;
        }
    }

    public void parseRoutes()
    {
        Log.i(TAG, "Parsing routes...");

        File routesFile = new File(baseDir, "routes.txt");

        try
        {
            CSVParser parser = CSVParser.parse(routesFile, Charset.forName("UTF-8"), CSVFormat.RFC4180.withHeader());

            for (final CSVRecord record : parser)
            {
                String routeId = record.get("route_id");
                String agencyId = record.get("agency_id");
                String routeShortName = record.get("route_short_name");
                String routeLongName = record.get("route_long_name");

                Route route = new Route(routeId, agencyId, routeShortName, routeLongName);

                routesMap.put(routeId, route);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public Map<String, Agency> getAgencies()
    {
        return agenciesMap;
    }

    public Map<String, Stop> getStops()
    {
        return stopsMap;
    }

    public List<Route> getRoutes() { return routes; }
}
