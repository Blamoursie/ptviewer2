package com.generalmagic.ptviewer.dbcreator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class Trip
{
    private long id;
    private String gtfsId;
    private String routeGtfsId;
    private int service;
    private long routeId;
    private List<StopTime> stopTimes;

    public Trip(String gtfsId, String routeGtfsId, int service)
    {
        this.gtfsId = gtfsId;
        this.service = service;
        this.routeGtfsId = routeGtfsId;
        stopTimes = new ArrayList<>();
    }

    public long getId()
    {
        return id;
    }

    public String getGtfsId()
    {
        return gtfsId;
    }

    public String getRouteGtfsId()
    {
        return routeGtfsId;
    }

    public int getService()
    {
        return service;
    }

    public long getRouteId()
    {
        return routeId;
    }

    public List<StopTime> getStopTimes() { return stopTimes; }

    public void setId(long id)
    {
        this.id = id;
    }

    public void setRouteId(long routeId)
    {
        this.routeId = routeId;
    }

    public void addStopTime(StopTime st)
    {
        stopTimes.add(st);
    }

    public void sortStopTimes()
    {
        Collections.sort(stopTimes, new Comparator<StopTime>()
        {

            @Override
            public int compare(StopTime o1, StopTime o2)
            {
                return o1.getStopSequence() - o2.getStopSequence();
            }
        });
    }

    public boolean sameStopsWith(Trip trip)
    {
        if(stopTimes.size() != trip.stopTimes.size())
            return false;

        for(int i=0;i<stopTimes.size();i++)
            if(!stopTimes.get(i).getStopGtfsId().equals(trip.stopTimes.get(i).getStopGtfsId()))
                return false;

        return true;
    }

    public String toString()
    {
        return String.format("%-20s with service %5d",gtfsId, service);
    }
}
