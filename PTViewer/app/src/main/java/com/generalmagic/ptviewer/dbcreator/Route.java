package com.generalmagic.ptviewer.dbcreator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class Route
{
    private long id;
    private String gtfsId;
    private String agencyGtfsId;
    private String routeShortName;
    private String routeLongName;
    private List<Trip> trips;

    public Route(String gtfsId, String agencyGtfsId, String routeShortName, String routeLongName)
    {
        this.gtfsId = gtfsId;
        this.agencyGtfsId = agencyGtfsId;
        this.routeShortName = routeShortName;
        this.routeLongName = routeLongName;
        trips = new ArrayList<>();
    }

    public long getId()
    {
        return id;
    }

    public String getGtfsId()
    {
        return gtfsId;
    }

    public String getRouteShortName()
    {
        return routeShortName;
    }

    public String getRouteLongName()
    {
        return routeLongName;
    }

    public String getAgencyGtfsId() { return agencyGtfsId; }

    public List<Trip> getTrips()
    {
        return trips;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public Route cloneRoute()
    {
        return new Route(gtfsId, agencyGtfsId, routeShortName, routeLongName);
    }

    public boolean hasNoTrips()
    {
        return trips.isEmpty();
    }

    public void addTrip(Trip trip)
    {
        trips.add(trip);
    }

    public boolean matchesTrip(Trip trip)
    {
        if(!gtfsId.equals(trip.getRouteGtfsId()))
            return false;

        if(trips.isEmpty())
            return true;

        Trip firstTrip = trips.get(0);

        return firstTrip.sameStopsWith(trip);

    }

    public void sortTrips()
    {
        Collections.sort(trips, new Comparator<Trip>()
        {

            @Override
            public int compare(Trip t1, Trip t2)
            {
                return t1.getStopTimes().get(0).getDeparture() - t2.getStopTimes().get(0).getDeparture();
            }
        });
    }
}
