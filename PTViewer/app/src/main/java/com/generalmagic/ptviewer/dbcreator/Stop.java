package com.generalmagic.ptviewer.dbcreator;

class Stop
{
    private long id;

    private String stopName;
    private double stopLat;
    private double stopLon;

    public Stop(String stopName, double lat, double lon)
    {
        this.stopName = stopName;
        this.stopLat = lat;
        this.stopLon = lon;
    }

    public long getId()
    {
        return id;
    }

    public String getStopName()
    {
        return stopName;
    }

    public double getLat()
    {
        return stopLat;
    }

    public double getLon()
    {
        return stopLon;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String toString()
    {
        return String.format("%s (with id %d) (%10.5f,%10.5f)", stopName, id, stopLat, stopLon);
    }
}
