package com.generalmagic.ptviewer.dbcreator;

class Agency
{
    private long id;
    private String agencyName;
    private String url;

    public Agency(String agencyName, String url)
    {
        this.agencyName = agencyName;
        this.url = url;
    }

    public long getId()
    {
        return id;
    }

    public String getAgencyName()
    {
        return agencyName;
    }

    public String getUrl()
    {
        return url;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String toString()
    {
        return String.format("%3d. %s", id, agencyName);
    }
}
