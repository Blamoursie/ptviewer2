package com.generalmagic.ptviewer;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.generalmagic.ptviewer.database.PTDatabaseHelper;

public class StopsCustomListActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>
{
    private PTDatabaseHelper databaseHelper;
    private StopsAdapter stopsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stops_custom_list);

        databaseHelper = new PTDatabaseHelper(this);

        //set onclick and onlongclick events for the list view
        ListView mListView = (ListView) findViewById(R.id.customStopsListView);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Toast.makeText(StopsCustomListActivity.this,
                        String.valueOf(id),
                        Toast.LENGTH_SHORT).show();
            }
        });

        mListView.setOnItemLongClickListener(
                new AdapterView.OnItemLongClickListener()
                {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
                    {
                        Toast.makeText(StopsCustomListActivity.this, "Records to delete = " + id, Toast.LENGTH_SHORT).show();
                        getSupportLoaderManager().restartLoader(0, null, StopsCustomListActivity.this);
                        return true;
                    }
                });

        //start loading
        getSupportLoaderManager().initLoader(0, null, this);
        stopsAdapter = new StopsAdapter(this, databaseHelper.getStopsListCursor(), 0);
        mListView.setAdapter(stopsAdapter);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {
        return new StopsLoader(this);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data)
    {
        stopsAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader)
    {
        stopsAdapter.swapCursor(null);
    }

    static class StopsLoader extends CursorLoader
    {
        Context mContext;

        public StopsLoader(Context context)
        {
            super(context);
            mContext = context;
        }

        @Override
        public Cursor loadInBackground()
        {
            PTDatabaseHelper db = new PTDatabaseHelper(mContext);
            return db.getStopsListCursor();
        }
    }

    class StopsAdapter extends CursorAdapter
    {
        public StopsAdapter(Context context, Cursor c, int flags)
        {
            super(context, c, flags);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent)
        {
            return LayoutInflater.from(context).inflate(R.layout.stops_custom_list_entry, parent, false);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor)
        {
            TextView nameTextView = (TextView) view.findViewById(R.id.name_entry);
            nameTextView.setText(cursor.getString(getCursor().getColumnIndex("stop_name")));

            TextView coordinatesTextView = (TextView) view.findViewById(R.id.coordinates_entry);

            double lat = cursor.getDouble(getCursor().getColumnIndex("stop_lat"));
            double lon = cursor.getDouble(getCursor().getColumnIndex("stop_lon"));

            coordinatesTextView.setText(String.format("(%.5f,%.5f)", lat, lon));
        }
    }

    @Override
    protected void onDestroy()
    {
        databaseHelper.close();
        super.onDestroy();
    }
}
