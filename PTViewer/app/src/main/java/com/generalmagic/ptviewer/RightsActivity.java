package com.generalmagic.ptviewer;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class RightsActivity extends AppCompatActivity
{
    private final int REQUEST_PERMISSION_SEND_SMS = 1;
    private final int REQUEST_PERMISSION_ACESS_FINE_LOCATION = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rights);


        if (!checkPermission(Manifest.permission.ACCESS_FINE_LOCATION ))
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION ))
            {
                showExplanation("ACCESS_FINE_LOCATION Permission Needed", "Access location", Manifest.permission.ACCESS_FINE_LOCATION , REQUEST_PERMISSION_ACESS_FINE_LOCATION);
            }
            else
            {
                requestPermission(Manifest.permission.ACCESS_FINE_LOCATION , REQUEST_PERMISSION_ACESS_FINE_LOCATION);
            }
        }
        else
        {
            logLocation();
        }
    }

    private void logLocation()
    {
        //GETTING CURRENT LOCATION
        // Acquire a reference to the system Location Manager
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        // Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                TextView textView = (TextView)findViewById(R.id.textView);
                textView.setText("Your location:\n"+location.getLatitude() + " , " + location.getLongitude());
                Log.i("ptviewer", "New location: " + location.getLatitude() + "," + location.getLongitude());
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {}

            public void onProviderEnabled(String provider) {}

            public void onProviderDisabled(String provider) {}
        };

        // Register the listener with the Location Manager to receive location updates
        if (checkPermission(Manifest.permission.ACCESS_FINE_LOCATION ))
        {
            Log.i("ptviewer", "Enabled requestLocationUpdates()");
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

        }
    }

    private boolean checkPermission(String permission)
    {
        int permissionCheck = ContextCompat.checkSelfPermission(this, permission);
        return (permissionCheck == PackageManager.PERMISSION_GRANTED);
    }

    private void showExplanation(String title,
                                 String message, final String permission,
                                 final int permissionRequestCode)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                requestPermission(permission, permissionRequestCode);
                                Log.i("ptviewer", "in onClick()");
                            }
                        });
        builder.create().show();
    }

    private void requestPermission(String permissionName, int permissionRequestCode)
    {
        ActivityCompat.requestPermissions(this, new String[]{permissionName}, permissionRequestCode);
    }

    /*
    1. Check to see whether you have the desired permissions. - ContextCompat.checkSelfPermission
    2. If not, check whether we should display the rationale (meaning, the request was
    previously denied). - ActivityCompat.requestPermissions
    3. Request the permission; only the OS can display the permission request. - ActivityCompat.shouldShowRequestPermissionRationale
    4. Handle the request response. - onRequestPermissionsResult
     */

    public void sendSMS(View view)
    {

        if (!checkPermission(Manifest.permission.SEND_SMS))
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.SEND_SMS))
            {
                showExplanation("Permission Needed", "Rationale", Manifest.permission.SEND_SMS, REQUEST_PERMISSION_SEND_SMS);
                Log.i("ptviewer", "After showExplanation()");
            }
            else
            {
                requestPermission(Manifest.permission.SEND_SMS, REQUEST_PERMISSION_SEND_SMS);
            }
        }
        else
        {
            Toast.makeText(RightsActivity.this, "Permission (already) Granted!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        switch (requestCode)
        {
            case REQUEST_PERMISSION_SEND_SMS:
            {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    Toast.makeText(RightsActivity.this, "Permission Granted!", Toast.LENGTH_SHORT).show();
                    Log.i("ptviewer","SEND_SMS Permission Granted!");
                }
                else
                {
                    Toast.makeText(RightsActivity.this, "Permission Denied!", Toast.LENGTH_SHORT).show();
                    Log.i("ptviewer","SEND_SMS Permission Denied!");
                }
                return;
            }
            case REQUEST_PERMISSION_ACESS_FINE_LOCATION:
            {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    Toast.makeText(RightsActivity.this, "Permission Granted!", Toast.LENGTH_SHORT).show();
                    Log.i("ptviewer","ACCESS_FINE_LOCATION Permission Granted!");
                    logLocation();
                }
                else
                {
                    Toast.makeText(RightsActivity.this, "Network state Permission Denied!", Toast.LENGTH_SHORT).show();
                    Log.i("ptviewer","ACCESS_FINE_LOCATION Permission Denied!");
                }
                return;
            }
        }
    }
}
