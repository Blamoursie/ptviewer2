package com.generalmagic.ptviewer;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.generalmagic.ptviewer.database.PTDatabaseHelper;
import com.generalmagic.ptviewer.database.Stop;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback
{

    private GoogleMap mMap;
    private ProgressDialog pDialog;

    private Map<Marker, Long> markersMap;//fiecarui marker ii atasez un id

    PTDatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        databaseHelper = new PTDatabaseHelper(this);

        markersMap = new HashMap<>();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mMap = googleMap;

        new StopsFromDbLoader().execute();
    }

    class StopsFromDbLoader extends AsyncTask<String, String, Collection<Stop>>
    {
        //before executing the background task
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();

            pDialog = new ProgressDialog(MapsActivity.this);
            pDialog.setMessage("Loading stops... Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        // working in background thread
        @Override
        protected Collection<Stop> doInBackground(String... urls)
        {
            return databaseHelper.getStops();
        }

        // after completing the background task
        @Override
        protected void onPostExecute(Collection<Stop> stops)
        {
            pDialog.dismiss();

            markersMap.clear();

            for (Stop stop : stops)
            {
                LatLng loc = new LatLng(stop.getLat(), stop.getLon());

                MarkerOptions markerOpts = new MarkerOptions().position(loc).title(stop.getStopName());

                Marker marker = mMap.addMarker(markerOpts);

                marker.showInfoWindow();

                markersMap.put(marker, stop.getId());
            }

            //LatLng brasovLatLon = new LatLng(45.651081, 25.604735);
            LatLng sanFranciscoLatLon = new LatLng(37.762022, -122.444377);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sanFranciscoLatLon, 10));

            mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener()
            {
                @Override
                public void onInfoWindowClick(Marker marker)
                {
                    Intent intent1 = new Intent(MapsActivity.this, StopDetailActivity.class);
                    Long id = markersMap.get(marker);
                    intent1.putExtra("stopid", id);
                    startActivity(intent1);
                }
            });
        }
    }

    @Override
    protected void onDestroy()
    {
        databaseHelper.close();
        super.onDestroy();
    }
}
