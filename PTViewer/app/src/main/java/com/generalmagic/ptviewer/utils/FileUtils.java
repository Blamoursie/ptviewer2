package com.generalmagic.ptviewer.utils;

import android.util.Log;

import java.io.File;

public class FileUtils
{
    public static void verifyFilesExist(File... files)
    {
        for(File file : files)
            if(file.exists())
                Log.i("", "File exist: " + file.getName());
            else
                Log.i("", "File doesn't exist: " + file.getName());
    }

    public static void clearDirectory(File dir)
    {
        for(File file : dir.listFiles())
            file.delete();
    }
}
