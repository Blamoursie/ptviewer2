package com.generalmagic.ptviewer.database;

class Agency
{
    private long _id;
    private String name;
    private String url;

    public Agency(long _id, String name, String url)
    {
        this._id = _id;
        this.name = name;
        this.url = url;
    }

    public long getId()
    {
        return _id;
    }

    public String getName()
    {
        return name;
    }

    public String getUrl()
    {
        return url;
    }
}
