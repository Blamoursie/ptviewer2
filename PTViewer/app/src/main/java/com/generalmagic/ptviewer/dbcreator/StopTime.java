package com.generalmagic.ptviewer.dbcreator;

class StopTime
{
    private long tripId;
    private long stopId;

    private int arrival;
    private int departure;
    private String stopGtfsId;
    private String tripGtfsId;
    private int stopSequence;

    public StopTime(int arrival, int departure, String stopGtfsId, String tripGtfsId, int stopSequence)
    {
        this.arrival = arrival;
        this.departure = departure;
        this.stopGtfsId = stopGtfsId;
        this.tripGtfsId = tripGtfsId;
        this.stopSequence = stopSequence;
    }

    public int getArrival()
    {
        return arrival;
    }

    public int getDeparture()
    {
        return departure;
    }

    public String getStopGtfsId()
    {
        return stopGtfsId;
    }

    public String getTripGtfsId()
    {
        return tripGtfsId;
    }

    public int getStopSequence()
    {
        return stopSequence;
    }

    public void setStopId(long stopId)
    {
        this.stopId = stopId;
    }

    public long getStopId()
    {
        return stopId;
    }

    public long getTripId()
    {
        return tripId;
    }

    public void setTripId(long tripId)
    {
        this.tripId = tripId;
    }
}
