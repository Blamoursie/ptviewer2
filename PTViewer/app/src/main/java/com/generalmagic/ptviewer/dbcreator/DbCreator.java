package com.generalmagic.ptviewer.dbcreator;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.generalmagic.ptviewer.database.PTDatabaseHelper;

import java.io.File;
import java.util.List;
import java.util.Map;

public class DbCreator
{
    private final static String TAG = "ptviewer";

    private PTDatabaseHelper dbHelper;
    private GtfsParser parser;

    public DbCreator(PTDatabaseHelper dbHelper)
    {
        this.dbHelper = dbHelper;

        parser = new GtfsParser();
    }

    public void createDatabase(File cacheDir)
    {
        parser.parse(cacheDir);

        writeDataToDatabase(parser);
    }

    private void writeDataToDatabase(GtfsParser parser)
    {
        writeData(parser.getAgencies(), parser.getStops(), parser.getRoutes());
    }

    private void writeData(Map<String, Agency> agencies, Map<String, Stop> stops, List<Route> routes)
    {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        Log.i(TAG, "Writing agencies...");

        for(Agency agency : agencies.values())
            insertAgency(db, agency);

        Log.i(TAG, "Writing stops...");

        for(Stop stop : stops.values())
            insertStop(db, stop);

        Log.i(TAG, "Writing " + routes.size() + " routes...");

        int i=0;
        for(Route route : routes)
        {
            i++;
            Log.i(TAG, "Write route " + i + "/" + routes.size() + " - " + route.getRouteShortName() + "->" + route.getRouteLongName());
            insertRoute(db, route, agencies, stops);
        }
    }

    private static void insertAgency(SQLiteDatabase db, Agency agency)
    {
        ContentValues agencyValues = new ContentValues();

        agencyValues.put("agency_name", agency.getAgencyName());
        agencyValues.put("agency_url", agency.getUrl());

        //return -1 if unsuccessful or a key generated value
        long generatedId = db.insert("agencies", null, agencyValues);

        agency.setId(generatedId);
    }

    private static void insertStop(SQLiteDatabase db, Stop stop)
    {
        ContentValues stopValues = new ContentValues();

        stopValues.put("stop_name", stop.getStopName());
        stopValues.put("stop_lat", stop.getLat());
        stopValues.put("stop_lon", stop.getLon());

        //return -1 if unsuccessful or a key generated value
        long generatedId = db.insert("stops", null, stopValues);

        stop.setId(generatedId);
    }

    private static void insertRoute(SQLiteDatabase db, Route route, Map<String, Agency> agenciesMap, Map<String, Stop> stops)
    {
        int stopTimes = 0;
        ContentValues values = new ContentValues();

        values.put("short_name", route.getRouteShortName());
        values.put("long_name", route.getRouteLongName());

        Agency agency = agenciesMap.get(route.getAgencyGtfsId());
        values.put("agency_id", agency.getId());

        //return -1 if unsuccessful or a key generated value
        long generatedRouteId = db.insert("routes", null, values);

        route.setId(generatedRouteId);

        db.beginTransaction();

        for(Trip trip : route.getTrips())
        {
            trip.setRouteId(generatedRouteId);

            values.clear();

            values.put("route_id", trip.getRouteId());
            values.put("service", trip.getService());

            //return -1 if unsuccessful or a key generated value
            long generatedTripId = db.insert("trips", null, values);

            //Log.i(TAG, "Trip inserted " + trip.getGtfsId());

            trip.setId(generatedTripId);
        }

        db.setTransactionSuccessful();
        db.endTransaction();

        db.beginTransaction();

        for(Trip trip : route.getTrips())
        {
            for(StopTime st : trip.getStopTimes())
            {
                stopTimes++;
                st.setTripId(trip.getId());
                st.setStopId(stops.get(st.getStopGtfsId()).getId());

                values.clear();

                values.put("trip_id", st.getTripId());
                values.put("arrival", st.getArrival());
                values.put("departure", st.getDeparture());
                values.put("stop_id", st.getStopId());

                //return -1 if unsuccessful or a key generated value
                db.insert("stop_times", null, values);

                //Log.i(TAG, "Stop time inserted: " + st.getStopGtfsId());
            }
        }

        db.setTransactionSuccessful();
        db.endTransaction();

        Log.i(TAG, "Wrote " + stopTimes + " stop times");
    }
}
