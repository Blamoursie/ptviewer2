package com.generalmagic.ptviewer.database;

public class Stop
{
    private long _id;
    private String stopName;
    private double lat;
    private double lon;

    public Stop(long _id, String stopName, double lat, double lon)
    {
        this._id = _id;
        this.stopName = stopName;
        this.lat = lat;
        this.lon = lon;
    }


    public long getId()
    {
        return _id;
    }

    public String getStopName()
    {
        return stopName;
    }

    public double getLat()
    {
        return lat;
    }

    public double getLon()
    {
        return lon;
    }
}
