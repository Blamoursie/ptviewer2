package com.generalmagic.ptviewer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class StopDetailActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stop_detail);

        Intent intent = getIntent();

        if(intent != null)
            Log.i("ptviewer", "Received intent value: " + intent.getLongExtra("stopid", 0));
        else
            Log.i("ptviewer", "Intent was: null");
    }
}
