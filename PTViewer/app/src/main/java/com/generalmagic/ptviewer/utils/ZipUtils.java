package com.generalmagic.ptviewer.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Created by miolu on 24-Apr-17.
 */

public class ZipUtils
{
    public static boolean unZip(File zipFile, File outputDir)
    {
        final int BUFFER_SIZE = 2048;

        // establish buffer for writing file
        byte data[] = new byte[BUFFER_SIZE];

        try
        {
            ZipFile zip = new ZipFile(zipFile);

            outputDir.mkdir();
            Enumeration<?> zipFileEntries = zip.entries();

            // Process each entry
            while (zipFileEntries.hasMoreElements())
            {
                // grab a zip file entry
                ZipEntry entry = (ZipEntry) zipFileEntries.nextElement();
                String currentEntry = entry.getName();
                File destFile = new File(outputDir, currentEntry);

                File destinationParent = destFile.getParentFile();

                // create the parent directory structure if needed
                destinationParent.mkdirs();

                if (!entry.isDirectory())
                {
                    try
                    {
                        BufferedInputStream is = new BufferedInputStream(zip.getInputStream(entry));
                        BufferedOutputStream dest = new BufferedOutputStream(new FileOutputStream(destFile), BUFFER_SIZE);
                        int currentByte;

                        // read and write until last byte is encountered
                        while ((currentByte = is.read(data, 0, BUFFER_SIZE)) != -1)
                            dest.write(data, 0, currentByte);

                        is.close();
                        dest.close();
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }

            zip.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
