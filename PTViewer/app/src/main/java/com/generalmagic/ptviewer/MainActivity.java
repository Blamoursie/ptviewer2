package com.generalmagic.ptviewer;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.generalmagic.ptviewer.database.PTDatabaseHelper;
import com.generalmagic.ptviewer.dbcreator.DbCreator;
import com.generalmagic.ptviewer.utils.Downloader;
import com.generalmagic.ptviewer.utils.FileUtils;
import com.generalmagic.ptviewer.utils.TimeUtils;
import com.generalmagic.ptviewer.utils.ZipUtils;

import java.io.File;
import java.util.Date;

public class MainActivity extends AppCompatActivity
{
    private static final String TAG = "com.gm.ptviewer";

    private final String GTFS_URL = "https://www.bart.gov/sites/default/files/docs/google_transit_20170325_v3.zip";
    private final String gtfsFileName = "google_transit_20170325_v3.zip";

    //private final String GTFS_URL = "http://www11.zippyshare.com/d/tHYZn8nb/50184/Brasov.zip";
    //private final String gtfsFileName = "Brasov.zip";

    private File cacheDir;
    private File gtfsFile;

    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cacheDir = getCacheDir();
        gtfsFile = new File(cacheDir, gtfsFileName);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }



    private int getCurrentDayOffset()
    {
        SharedPreferences sp = getSharedPreferences("my.preferences", Context.MODE_PRIVATE);

        long databaseGenerationDate = sp.getLong("databaseGenerationDate", -1);

        if (databaseGenerationDate == -1)
            return -1;

        Date currentDate = TimeUtils.getCurrentDate();

        return TimeUtils.getDifferenceDays(new Date(databaseGenerationDate), currentDate);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_download)
        {
            deleteDatabase("pt");

            new DatabaseCreatorTask().execute(GTFS_URL); //extinde assignTask, downloadeaza pe un fir de executie separat
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class DatabaseCreatorTask extends AsyncTask<String, String, String>
    {
        //before executing the background task
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();

            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Downloading... Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        // working in background thread
        @Override
        protected String doInBackground(String... urls)
        {
            Downloader.download(urls[0], gtfsFile);

            publishProgress("Unzipping... Please wait...");

            ZipUtils.unZip(gtfsFile, cacheDir);

            publishProgress("Parse and create db... Please wait...");

            PTDatabaseHelper databaseHelper = new PTDatabaseHelper(MainActivity.this);

            DbCreator dbCreator = new DbCreator(databaseHelper);
            dbCreator.createDatabase(cacheDir);

            databaseHelper.close();

            publishProgress("Clear directory... Please wait...");

            FileUtils.clearDirectory(cacheDir);

            publishProgress("Saving preferences...Please wait...");

            SharedPreferences sp = getSharedPreferences("my.preferences", Context.MODE_PRIVATE);

            SharedPreferences.Editor e = sp.edit();

            Date currentDate = TimeUtils.getCurrentDate();

            e.putLong("databaseGenerationDate", currentDate.getTime());

            e.commit();

            return null;
        }

        @Override
        protected void onProgressUpdate(String... message)
        {
            pDialog.setMessage(message[0]);
        }

        // after completing the background task (result is what doInBackground() returned)
        @Override
        protected void onPostExecute(String result)
        {
            pDialog.dismiss();

            Toast.makeText(MainActivity.this, "Download finished", Toast.LENGTH_LONG).show();
        }
    }

    public void viewStops(View view)
    {
        Intent intent = new Intent(this, StopsListActivity.class);
        startActivity(intent);
    }

    public void viewRoutes(View view){
        Intent intent=new Intent(this, RoutesListActivity.class);
        startActivity(intent);
    }

    public void viewCustomStops(View view)
    {
        Intent intent = new Intent(this, StopsCustomListActivity.class);
        startActivity(intent);
    }

    public void openMap(View view)
    {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }

    public void gotoRightsActivity(View view)
    {
        Intent intent = new Intent(this, RightsActivity.class);
        startActivity(intent);
    }
}
