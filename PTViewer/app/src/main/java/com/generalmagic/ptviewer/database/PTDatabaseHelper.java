package com.generalmagic.ptviewer.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collection;

public class PTDatabaseHelper extends SQLiteOpenHelper
{
    private static final String DB_NAME = "pt"; // the name of our database
    private static final int DB_VERSION = 1; // the version of the database

    private static final String TAG = "ptviewer";

    public PTDatabaseHelper(Context context)
    {
        super(context, DB_NAME, null, DB_VERSION);
    }

    //when I first run the application
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE agencies (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "agency_name TEXT, "
                + "agency_url TEXT);");

        db.execSQL("CREATE TABLE stops (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "stop_name TEXT, "
                + "stop_lat real, "
                + "stop_lon real);");

        db.execSQL("CREATE TABLE routes (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "short_name TEXT, "
                + "long_name TEXT, "
                + "agency_id INTEGER);");

        db.execSQL("CREATE TABLE trips (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "route_id INTEGER, "
                + "service INTEGER);");

        db.execSQL("CREATE TABLE stop_times (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "trip_id INTEGER, "
                + "arrival INTEGER, "
                + "departure INTEGER, "
                + "stop_id INTEGER);");
    }

    //when the database must be upgraded (after release)
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
    }

    public Cursor getStopsListCursor()
    {
        SQLiteDatabase db = getReadableDatabase();

        //String[] projection = {"1"};
        //would work if we have something like where _id < ?

//      String query = "SELECT _id, stop_name, stop_lat, stop_lon " +
//                "FROM stops ORDER BY stop_name ASC";

        String query = "SELECT * " +
                "FROM stops ORDER BY stop_name ASC";

        int route_id=3;
//        String query="Select s._id from stops s inner join stop_times st on s._id=st.stop_id " +
//                "inner join trips t on  " +
//                "st.trip_id= t._id where route_id= 8607;"

//        String query="Select stop_id from  stop_times " +
//                "inner join trips on  " +
//                "trip_id= _id where route_id= 8607;";

        //String query="SELECT stop_id FROM stop_times;";
        return db.rawQuery(query, null);
    }

    public Cursor getStopsOfRoute()
    {
        SQLiteDatabase db = getReadableDatabase();

        int route_id=1;
        //String query="Select stop_id from stop_times inner join trips on _id where route_id= "+route_id+";";

        String query="SELECT stop_id FROM stop_times;";
        return db.rawQuery(query, null);
    }

    public Cursor getRoutesListCursor()
    {
        SQLiteDatabase db = getReadableDatabase();

        //String[] projection = {"1"};
        //would work if we have something like where _id < ?

        String query = "SELECT _id, long_name, agency_id " +
                "FROM routes ORDER BY long_name ASC";

        //int route_id=8067;
//        String query = "SELECT _id, long_name, agency_id " +
//               "FROM routes ORDER BY long_name ASC";

//        int route_id=12;
//        String query="Select stop_name from stops;";
        return db.rawQuery(query, null);
    }

    public Collection<Agency> getAgencies()
    {
        SQLiteDatabase db = getReadableDatabase();

        String[] projection = {"_id", "agency_name", "agency_url"};

        Cursor cursor = db.query("agencies", projection, null, null, null, null, null);

        Collection<Agency> agencies = new ArrayList<>();

        while(cursor.moveToNext())
        {
            long id = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
            String agencyName = cursor.getString(cursor.getColumnIndexOrThrow("agency_name"));
            String url = cursor.getString(cursor.getColumnIndexOrThrow("agency_url"));

            Agency agency = new Agency(id, agencyName, url);

            agencies.add(agency);
        }

        cursor.close();

        db.close();

        return agencies;
    }

    public void printRoutesForStop(int stopId)
    {
        SQLiteDatabase db = getReadableDatabase();

        String[] projection = {"2"};

        //Cursor cursor = db.rawQuery("select _id, long_name from routes where _id in (select route_id from trips t where _id in (select distinct(trip_id) from stop_times where stop_id = ?))", projection);

        Cursor cursor = db.rawQuery("select _id, long_name from routes where _id in (select route_id from trips t inner join stop_times st on t._id = st.trip_id and st.stop_id = ?)", projection);

        while(cursor.moveToNext())
        {
            long id = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
            String longName = cursor.getString(cursor.getColumnIndexOrThrow("long_name"));
            Log.i(TAG, "id " + id + " for route " + longName);
        }

        cursor.close();

        db.close();
    }

    public Collection<Stop> getStops()
    {
        Collection<Stop> stops = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();

        String[] stopsProjection = {"_id", "stop_name", "stop_lat", "stop_lon"};

        Cursor cursor = db.query("stops", stopsProjection, null, null, null, null, null);

        while(cursor.moveToNext())
        {
            long id = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
            String stopName = cursor.getString(cursor.getColumnIndexOrThrow("stop_name"));
            double lat = cursor.getDouble(cursor.getColumnIndexOrThrow("stop_lat"));
            double lon = cursor.getDouble(cursor.getColumnIndexOrThrow("stop_lon"));

//            Log.i("ptviewer", "Read record: " + id + "," + stopName + "(" + lat + "," + lon + ")");

            Stop stop = new Stop(id, stopName, lat, lon);

            stops.add(stop);
        }

        cursor.close();

        db.close();

        return stops;

    }
}
