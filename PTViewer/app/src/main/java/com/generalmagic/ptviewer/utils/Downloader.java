package com.generalmagic.ptviewer.utils;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class Downloader
{
    public static void download(String link, File destFile)
    {
        int count;
        try
        {
            URL url = new URL(link);

            URLConnection connection = url.openConnection();

            connection.connect();

            InputStream input = new BufferedInputStream(url.openStream(), 8192);
            OutputStream output = new BufferedOutputStream(new FileOutputStream(destFile));

            byte data[] = new byte[1024];

            long total = 0;
            while ((count = input.read(data)) != -1)
            {
                total += count;
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();
        }
        catch (Exception e)
        {
            Log.e("Error: ", e.getMessage());
        }
    }
}
