package com.generalmagic.ptviewer;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.generalmagic.ptviewer.database.PTDatabaseHelper;
import com.generalmagic.ptviewer.utils.TimeUtils;

import java.util.Date;

public class StopsListActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>
{
    private StopsAdapter stopsAdapter;
    private PTDatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stops_list);

        databaseHelper = new PTDatabaseHelper(this);

        //set onclick and onlongclick events for the list view
        ListView mListView = (ListView) findViewById(R.id.stopsListView);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Toast.makeText(StopsListActivity.this,
                        String.valueOf(id),
                        Toast.LENGTH_SHORT).show();
            }
        });

        mListView.setOnItemLongClickListener(
                new AdapterView.OnItemLongClickListener()
                {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
                    {
                        Toast.makeText(StopsListActivity.this, "Records to delete = " + id, Toast.LENGTH_SHORT).show();
                        getSupportLoaderManager().restartLoader(0, null, StopsListActivity.this);
                        return true;
                    }
                });

        //start loading
        getSupportLoaderManager().initLoader(0, null, this);
        stopsAdapter = new StopsAdapter(this, databaseHelper.getStopsListCursor(), 0);
        mListView.setAdapter(stopsAdapter);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {
        return new StopsLoader(this);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data)
    {
        stopsAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader)
    {
        stopsAdapter.swapCursor(null);
    }

    public static class StopsLoader extends CursorLoader
    {
        Context mContext;

        public StopsLoader(Context context)
        {
            super(context);
            mContext = context;
        }

        @Override
        public Cursor loadInBackground()
        {
            PTDatabaseHelper db = new PTDatabaseHelper(mContext);
            return db.getStopsListCursor();
        }
    }

    public class StopsAdapter extends CursorAdapter
    {
        public StopsAdapter(Context context, Cursor c, int flags)
        {
            super(context, c, flags);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent)
        {
            return LayoutInflater.from(context).inflate(
                    android.R.layout.simple_list_item_1, parent,
                    false);
        }

        @Override
        public void bindView(View view, Context context, Cursor
                cursor)
        {
            TextView textView = (TextView) view.findViewById(android.R.id.text1);
            textView.setText(cursor.getString(getCursor().getColumnIndex("stop_name")));
        }
    }

    @Override
    protected void onDestroy()
    {
        databaseHelper.close();
        super.onDestroy();
    }

}
